# -*- coding: utf-8 -*-
import json
import os
import socket
import requests
import time
import datetime
import sys

TOKEN = 'YOUR GROUP TOKEN HERE(MANAGE MUST BE ENABLED)'
GROUP_ID = 'YOUR GROUP ID HERE'

def is_number(s):
	try:
		int(s)
		return True
	except ValueError:
		return False

def authorize():
	try:
		response = requests.get('https://api.vk.com/method/groups.getLongPollServer?access_token={}&v=5.74&group_id={}'.format(TOKEN,GROUP_ID))
		content = response.content.decode('utf-8')
		d = json.loads(content)
		return d['response']['key'],d['response']['server'],d['response']['ts']
	except:
		print('Error can\'t connect to the vk.com. Exiting...')
		sys.exit()

def analyze(data,userid):
	now = datetime.datetime.now()
	if data == '':
		return 'No such command!'
	r = data.split()
	i = len(r)
	s = ""
	r[0] = r[0].lower()
	if(r[0] == 'help'):
		return 'YOUR HELP HERE'
	if(r[0] == 'news'):
		return news()
	if(r[0] == 'пинг' or r[0] == 'ping'):
		return 'It\'s working!'
	if r[0] == 'delnews' and prava(userid) == 1:
		if os.path.exists('news.txt'):
			os.remove('news.txt')
		return 'Successfully deleted news'
	if i < 3 and r[0] != 'viewinfo' and r[0] != 'дз' and r[0] != 'delday' and r[0] != 'delcontrol' and r[0] != 'delhomework' and r[0] != 'addnews' and r[0] != 'delatach':
		return 'Enter arguments for command!'
	if i < 2:
		return 'Enter arguments for command!'
	for x in range(2,i):
		s = s + r[x] + ' '
	s = s[0:-1]
	s = s.replace('\"','\'')
	s = s.replace('+','%2B')
	s = s.replace('#','№')
	r[1] = r[1].lower()
	if r[1] == 'today':
		p = str(now.month)
		if now.month < 10:
			p = '0' + str(now.month)
		k = str(now.day)
		if now.day < 10:
			k = '0' + str(now.day)
		r[1] = k + '.' + p + '.' + str(now.year % 100)
	if r[1] == 'tommorow':
		tom = datetime.date.today() + datetime.timedelta(days=1)
		p = str(tom.month)
		if tom.month < 10:
			p = '0' + str(tom.month)
		k = str(tom.day)
		if tom.day < 10:
			k = '0' + str(tom.day)
		r[1] = k + '.' + p + '.' + str(tom.year % 100)
	if r[0] == "addcontrol" and prava(userid) == 1:
		writeDay(r[1],1,s)
		return 'Added control work ' + s + ' on  ' + r[1]
	elif r[0] == "addhomework" and prava(userid) == 1:
		writeDay(r[1],2,s)
		return 'Added homework ' + s + ' on  ' + r[1]
	elif r[0] == "addatach" and prava(userid) == 1:
		writeDay(r[1],3,s)
		return 'Added atachment ' + s + ' on  ' + r[1]
	elif r[0] == "viewinfo":
		return getH(r[1],userid)
	elif r[0] == 'delday' and prava(userid) == 1:
		os.remove('{}.json'.format(r[1]))
		return 'Everything on  ' + r[1] + ' has been deleted'
	elif r[0] == 'delcontrol' and i == 2 and prava(userid) == 1:
		return punktOut(1,r[1])
	elif r[0] == 'delhomework' and i == 2 and prava(userid) == 1:
		return punktOut(2,r[1])
	elif r[0] == 'delatach' and i == 2 and prava(userid) == 1:
		return punktOut(3,r[1])
	elif r[0] == 'delcontrol' and prava(userid) == 1:
		return delDay(r[1],1,r[2])
	elif r[0] == 'delhomework' and prava(userid) == 1:
		return delDay(r[1],2,r[2])
	elif r[0] == 'delatach' and prava(userid) == 1:
		return delDay(r[1],3,r[2])
	elif r[0] == 'addnews' and prava(userid) == 1:
		f = open('news.txt','w')
		f.write(r[1] + ' ' + s)
		f.close()
		return 'Succesfully added news'
	elif prava(userid) == 0:
		return 'Insufficient privileges!'
	else:
		return 'Insufficient privileges!'

def prava(user):
	try:
		f = open('admins.json')
		d = json.load(f)
		return d[str(user)]
	except:
		return 0

def news():
	if not os.path.exists('news.txt'):
		return 'No news'
	f = open('news.txt')
	s = f.read()
	f.close()
	return s
	
def checkForUpdates(key,server,ts):
	try:
		response = requests.get('{}?act=a_check&key={}&ts={}&wait=25&mode=2&version=2'.format(server,key,ts))
		content = response.content.decode('utf-8')
		d = json.loads(content)
	except:
		return authorize()
	boo = ''
	keynew,servernew,tsn,boo = checkFailed(d,key,server,ts)
	if boo == 'yes' and key != '':
		return keynew,servernew,tsn
	tsnew = int(d['ts'])
	i = tsnew - ts
	for x in range(0, i):
		writeMessage(analyze(d['updates'][x]['object']['body'],d['updates'][x]['object']['user_id']),d['updates'][x]['object']['user_id'])
	return key,server,tsnew

def checkFailed(d,key,server,ts):
	try:
		if d['failed'] == 2 or d['failed'] == 3:
			key,server,ts = authorize()
			return key,server,ts,'yes'
		if d['failed'] == 1:
			return key,server,d['ts'],'yes'
		return key,server,ts,'no'
	except:
		return key,server,ts,'no'
def writeMessage(text,user):
	try:
		response = requests.get('https://api.vk.com/api.php?oauth=1&method=messages.send&user_id={}&access_token={}&v=5.74&message={}'.format(user,TOKEN,text))
	except:
		print('Can\'t write message. Exiting....')
		sys.exit()
def getH(date,userid):
	if not os.path.exists('{}.json'.format(date)):
		return 'There is nothing on ' + date
	with open('{}.json'.format(date)) as json_data:
		d = json.load(json_data)
		i = d["count"]
		s = ""
		s = s + date + "There is control works on:" + '\n'
		if i == 0:
			s = s + 'No control works' + '\n'
		for x in range (0,i):
			s = s + d["control"][x] + '\n'
		s = s + "There is control works on:" + '\n'
		i = d["counth"]
		if i == 0:
			s = s + 'No homework' + '\n'
		for x in range (0,i):
			s = s + d["homework"][x] + '\n'
		r = d['countd']
		for x in range (0,r):
			sendAttach(d["doc"][x],userid)
	return s

def sendAttach(id,user):
	response = requests.get('https://api.vk.com/api.php?oauth=1&method=messages.send&user_id={}&access_token={}&v=5.74&attachment={}&message=Фотографии и документы'.format(user,TOKEN,id))

def punktOut(type,date):
	if not os.path.exists('{}.json'.format(date)):
		return 'There is nothing on ' + date
	if type == 1:
		old = open('{}.json'.format(date))
		d = json.load(old)
		i = d["count"]
		j = 0
		s = 'Choose one of paragraphs :\n'
		for x in range (0,i):
			s = s + str(j) + '- ' + d["control"][x] + '\n'
			j = j + 1
		return s
	if type == 2:
		old = open('{}.json'.format(date))
		d = json.load(old)
		i = d["counth"]
		j = 0
		s = 'Choose one of paragraphs :\n'
		for x in range (0,i):
			s = s + str(j) + '- ' + d["homework"][x] + '\n'
			j = j + 1
		return s
	if type == 3:
		old = open('{}.json'.format(date))
		d = json.load(old)
		i = d["countd"]
		j = 0
		s = 'Choose one of paragraphs :\n'
		for x in range(0,i):
			s = s + str(j) + '- ' + d["doc"][x] + '\n'
			j = j + 1
		return s
def writeDay(date,type,input):
	r = ""
	s = ""
	k = ""
	i = 0
	j = 0
	b = 0
	if(os.path.exists('{}.json'.format(date))):
		old = open('{}.json'.format(date))
		d = json.load(old)
		i = d["count"]
		for x in range (0,i):
			s = s + '"' + d["control"][x] + '",'
		j = d["counth"]
		for x in range (0,j):
			r = r + '"' + d["homework"][x] + '",'
		b = d["countd"]
		for x in range (0,b):
			k = k + '"' + d["doc"][x] + '",'
	if type == 1 :
		r = r[0:-1]
		k = k[0:-1]
		f = open('{}.json'.format(date),'w')
		f.write('{')
		f.write('"count":{},"control":['.format(i+1))
		f.write(s)
		f.write('"{}"],'.format(input))
		f.write('"counth":{}, "homework":['.format(j))
		f.write(r);
		f.write('], "countd":{},"doc":['.format(b))
		f.write(k)
		f.write("]}")
		f.close()
	if type == 2:
		s = s[0:-1]
		k = k[0:-1]
		f = open('{}.json'.format(date),'w')
		f.write('{')
		f.write('"count":{},"control":['.format(i))
		f.write(s)
		f.write('],')
		f.write('"counth":{}, "homework":['.format(j+1))
		f.write(r);
		f.write('"{}"],'.format(input))
		f.write('"countd":{},"doc":['.format(b))
		f.write(k)
		f.write("]}")
		f.close()
	if type == 3:
		s = s[0:-1]
		r = r[0:-1]
		f = open('{}.json'.format(date),'w')
		f.write('{')
		f.write('"count":{}, "control":['.format(i))
		f.write(s)
		f.write('], "counth":{},"homework":['.format(j))
		f.write(r)
		f.write('], "countd":{},"doc":['.format(b+1))
		f.write(k)
		f.write('"{}"]'.format(input))
		f.write('}')
		f.close()
	pass

def delDay(date,type,punkt):
	if(os.path.exists('{}.json'.format(date)) and is_number(punkt)):
		punkt = int(punkt)
		f = open('{}.json'.format(date),'r')
		d = json.load(f)
		f.close()
		f = open('{}.json'.format(date),'w')
		s = ""
		if type == 1:
			i = d["count"]
			f.write('{')
			f.write('"count":{},"control":['.format(i-1))
			for x in range (0,i):
				if(x != punkt):
					s = s + '"' + d["control"][x] + '",'
			s = s[0:-1]
			f.write(s)
			f.write("],")
			j = d["counth"]
			f.write('"counth":{}, "homework":['.format(j))
			s = ""
			for x in range (0,j):
				s = s + '"' + d["homework"][x] + '",'
			s = s[0:-1]
			f.write(s)
			s = ""
			r = d["countd"]
			f.write('], "countd":{},"doc":['.format(r))
			for x in range(0,r):
				s = s + '"' + d["doc"][x] + '",'
			s = s[0:-1]
			f.write(s)
			f.write("]}")
			f.close()
		if type == 2:
			i = d["count"]
			f.write('{')
			f.write('"count":{},"control":['.format(i))
			for x in range (0,i):
				s = s + '"' + d["control"][x] + '",'
			s = s[0:-1]
			f.write(s)
			f.write("],")
			j = d["counth"]
			f.write('"counth":{}, "homework":['.format(j-1))
			s = ""
			for x in range (0,j):
				if(x != punkt):
					s = s + '"' + d["homework"][x] + '",'
			s = s[0:-1]
			f.write(s)
			s = ""
			r = d["countd"]
			f.write('],"countd":{},"doc":['.format(r))
			for x in range(0,r):
				s = s + '"' + d["doc"][x]+'",'
			s = s[0:-1]
			f.write(s)
			f.write("]}")
			f.close()
		if type == 3:
			i = d["count"]
			f.write("{")
			f.write('"count":{},"control":['.format(i))
			for x in range(0,i):
				s = s + '"' + d["control"][x] +  '",'
			s = s[0:-1]
			f.write(s)
			s = ""
			i = d["counth"]
			f.write('], "counth":{}, "homework":['.format(i))
			for x in range(0,i):
				s = s + '"' + d["homework"][x] + '",'
			s = s[0:-1]
			f.write(s)
			s = ""
			i = d["countd"]
			f.write('], "countd":{}, "doc":['.format(i-1))
			for x in range(0,i):
				if x != punkt:
					s = s + '"' + d["doc"][x] + '",'
			s = s[0:-1]
			f.write(s)
			f.write(']}')
			f.close()
	else:
		return 'There is nothing on ' + date
	return 'Succesfully deleted'

key, server, ts = authorize()
while True:
	key,server,ts = checkForUpdates(key,server,ts)
	time.sleep(1)
