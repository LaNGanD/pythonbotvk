# pythonbotvk
This is Python bot for vk.com, that can be used as a collective notebook.

Right now it is used as a homework bot.

Bot is based on VK long poll API.

Commands are:

+ help - show editable help
+ news - show editable news
+ addnews - set new news
+ ping - test bot
+ delnews - fully delete news
+ viewinfo or <date> - view homework or any info on <date>(if you want you can use words tommorow, today instead of date)
+ addcontrol or addhomework <date> <info> - add some info to the date(if you want you can use words tommorow, today instead of date)
+ addatach <date> <id> - add some atachment(it must be public atachment or it must be atachment in your vk group)(id format is photo-123456789_123456789)
+ delday <date> - delete everything on date
+ delcontrol or delhomework <date> - show numbers of homework for next command
+ delcontrol or delhomework <date> <number> - delete number from control or homework
+ delatach - similarly

I also included script to restart bot automaticly(startup). You will need to download nohup to use it.
admins.json file needed to check admin status of user. Don't delete it
